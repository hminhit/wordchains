Program - Usage
---------------

	Usage: /bin/wordchains.php --start=START --end=END -steps=STEPS --import=FILE

The command-line program accepts four parameters, *START*, *FINISH*, and
--start: The first word.

--end: The last word.

--steps: The number of steps, is optional.

--import: The import data only supports XML file, supports multi file by comma(,).