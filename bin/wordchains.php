#! /usr/bin/php
<?php
define('SHELL_NAME', basename(__FILE__));
define('XML_EXT', 'xml');
require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$agrvInput = \WordChains\Helper\ArgvInput::parseInput();

try {
    if (isset($agrvInput['import'])) {
        $filename = $agrvInput['import'];
        if (empty($filename)) {
            throw new \Exception('');
        }

        $filename = explode(',', $filename);
        $isInValidExt = false;
        foreach ($filename as $file) {
            $ext = \WordChains\Helper\Helper::getExtension($file);
            if ($ext != XML_EXT) {
                $isInValidExt = true;
            }
        }
        if ($isInValidExt) {
            throw new \Exception('The import data only supports XML file.');
        }

        $xml = new \WordChains\Importer\XML($filename);
        $xml->import();
    }

    $start = $end = $steps = '';
    if (isset($agrvInput['start'])) {
        $start = $agrvInput['start'];
    }

    if (isset($agrvInput['end'])) {
        $end = $agrvInput['end'];
    }

    if (isset($agrvInput['steps'])) {
        $steps = $agrvInput['steps'];
    }

    $solver = \WordChains\WordChains::solve($start, $end, $steps);

} catch (\Exception $exc) {
    echo $exc->getMessage();
}

?>