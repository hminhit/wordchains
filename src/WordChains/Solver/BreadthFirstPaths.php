<?php
namespace WordChains\Solver;

use WordChains\Graph;

class BreadthFirstPaths
{
    private $marked;
    private $distTo;
    private $edgeTo;

    /**
     * Computes the shortest path between the source vertex s
     *
     * @param Graph $graph
     * @param $s
     */
    public function __construct(Graph $graph, $s)
    {
        for ($i = 0; $i < $graph->getV(); $i++) {
            $this->marked[$i] = false;
            $this->distTo[$i] = PHP_INT_MAX;
            $this->edgeTo[$i] = null;
        }
        $this->solve($graph, $s);
    }

    /**
     * Is there a path between the source vertex s and vertex v
     *
     * @param $v
     * @return boolean
     */
    public function getPathTo($v)
    {
        return $this->marked[$v];
    }

    /**
     * Returns the number of edges in a shortest path between the source vertex s
     *
     * @param $v
     * @return int
     */
    public function getDistTo($v)
    {
        return $this->distTo[$v];
    }

    /**
     *  Returns previous edge on shortest s-v path
     *
     * @param $v
     * @return int
     */
    public function getEdgeTo($v)
    {
        return $this->edgeTo[$v];
    }

    /**
     * Returns a shortest path between the source vertex s
     *
     * @param $v
     * @return array
     */
    public function pathTo($v)
    {
        $path = array();
        for ($x = $v; $this->distTo[$x] != 0; $x = $this->edgeTo[$x]) {
            $path[] = $x;
        }
        $path[] = $x;

        return $path;
    }

    /**
     * Run breadth first search on an graph.
     *
     * @param $graph
     * @param $s
     */
    public function solve($graph, $s)
    {
        $sq = new \SplQueue();
        $this->distTo[$s] = 0;
        $this->marked[$s] = true;
        $sq->enqueue($s);
        while (!$sq->isEmpty()) {
            $v = $sq->dequeue();
            foreach ($graph->getAdj($v) as $w) {
                if (!$this->marked[$w]) {
                    $this->edgeTo[$w] = $v;
                    $this->distTo[$w] = $this->distTo[$v] + 1;
                    $this->marked[$w] = true;
                    $sq->enqueue($w);
                }
            }
        }
    }
}