<?php

namespace WordChains\Helper;

class Helper
{
    const DATAFILE_POSTFIX = '-letters';
    const DATAFILE_EXTENSION = '.txt';

    /**
     * Get extension of file
     *
     * @param $filename
     * @return string
     */
    public static function getExtension($filename)
    {
        $fileInfo = new \SplFileInfo($filename);

        return $fileInfo->getExtension();
    }

    /**
     * Get root dir program path
     *
     * @return string
     */
    public static function getRootDir()
    {
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR;
    }

    /**
     * Get root dir data
     *
     * @return string
     */
    public static function getRootDirData()
    {
        return self::getRootDir() . 'data' . DIRECTORY_SEPARATOR;
    }

    /**
     * Get data source path
     *
     * @param $letter
     * @return string
     */
    public static function getDataSourceLetter($letter)
    {
        return self::getRootDirData() . $letter . self::DATAFILE_POSTFIX;
    }

    /**
     * Write log to file
     *
     * @param $data
     * @return int
     */
    public static function writeLog($data)
    {
        return file_put_contents(self::getRootDir() . 'logs' . DIRECTORY_SEPARATOR . 'log.txt', print_r($data, true));
    }

    /**
     * Returns true if folder is empty
     *
     * @param $dir
     * @return bool
     */
    public static function isDirEmpty($dir)
    {
        $iterator = new \FilesystemIterator($dir);
        $isDirEmpty = !$iterator->valid();

        return $isDirEmpty;
    }
}