<?php
namespace WordChains\Helper;

class ArgvInput
{
    /**
     * Returns argv params
     *
     * @param array $inputs
     * @return array
     */
    public static function parseInput(array $inputs = null)
    {
        if ($inputs == null) {
            $inputs = $_SERVER['argv'];
        }

        $params = array();
        foreach ($inputs as $key => $input) {
            $param = explode('=', $input);
            if (count($param) == 2) {
                $param[0] = str_replace('-', '', $param[0]);
                $params[$param[0]] = $param[1];
            }
        }

        return $params;
    }
}