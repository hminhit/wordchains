<?php
namespace WordChains\Importer;

use WordChains\Helper\Helper;

class XML
{
    private $filename;
    private $source;
    const DATAFILE_POSTFIX = '-letters';
    const DATAFILE_EXTENSION = '.json';

    public function __construct($filename, $source = '')
    {
        $this->filename = $filename;
        if (!$source) {
            $this->source = Helper::getRootDir() . 'data' . DIRECTORY_SEPARATOR;
        }
    }

    /**
     * Import XML to data
     *
     * @throws \Exception
     */
    public function import()
    {
        $letters = array();
        $xmlString = '';
        // original file contents
        foreach ($this->filename as $file) {
            $xmlString .= file_get_contents($file);
        }
        // find words in original file contents
        preg_match_all("/<ent>(?P<words>.*?)<\/ent>/", $xmlString, $matches);
        if (isset($matches['words'])) {
            $words = array_unique($matches[1]);
            if (!empty($words)) {
                foreach ($words as $word) {
                    $letters[strlen($word)][] = strtolower($word);
                }
            }
        }
        $this->createFileData($letters);
    }

    /**
     * Create file data follow by letter
     *
     * @param array $letters
     * @throws \Exception
     */
    private function createFileData($letters = array())
    {
        if (!empty($letters)) {
            foreach ($letters as $letter => $words) {
                $filename = $this->source . $letter . self::DATAFILE_POSTFIX . self::DATAFILE_EXTENSION;
                $handle = fopen($filename, 'w');
                if (!$handle) {
                    throw new \Exception('Unable to open file.');
                }
                $data = json_encode($words);
                fwrite($handle, $data);
                fclose($handle);
            }
        }
    }
}