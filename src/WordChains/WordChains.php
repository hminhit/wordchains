<?php
namespace WordChains;

use WordChains\Helper\Helper;

class WordChains
{

    /**
     * Solve word chain puzzle
     *
     * @param string $start
     * @param string $end
     * @param integer $steps
     * @throws \Exception
     */
    public static function solve($start, $end, $steps = null)
    {
        if (strlen($start) != strlen($end)) {
            throw new \Exception("Words have different lengths.");
        }
        //  Read a list of strings, all of the same length.
        $startLetDir = Helper::getDataSourceLetter(strlen($start));
        $startLetFile = $startLetDir . Helper::DATAFILE_EXTENSION;
        if (!file_exists($startLetFile)) {
            throw new \Exception('Could not find ' . $start . ' in dictionary.');
        }
        // Building graph.
        $words = json_decode(file_get_contents($startLetFile), true);
        $graph = new Graph(count($words));
        foreach ($words as $word1) {
            foreach ($words as $word2) {
                if (strcmp($word1, $word2) < 0 && self::isNeighbor($word1, $word2)) {
                    $graph->addEdge(array_search($word1, $words), array_search($word2, $words));
                }
            }
        }
        // Run breadth first search.
        $startV = array_search($start, $words);
        $endV = array_search($end, $words);
        $bfs = new Solver\BreadthFirstPaths($graph, $endV);
        $step = 0;
        $chains = array();
        if ($bfs->getPathTo($startV)) {
            $path = $bfs->pathTo($startV);
            if (!empty($path)) {
                foreach ($path as $v) {
                    $step++;
                    if(!empty($steps) && $steps == $step) {
                        break;
                    } else {
                        $chains[] = $words[$v];
                    }
                }
            }
        }
        if(!empty($chains)) {
            echo implode(' - ', $chains);
        }
    }

    /**
     * Check two strings differ in exactly one letter
     *
     * @param  string $a
     * @param  string $b
     * @return boolean
     */
    private static function isNeighbor($a, $b)
    {
        $differ = 0;
        for ($i = 0; $i < strlen($a); $i++) {
            if ($a[$i] != $b[$i]) {
                $differ++;
                if ($differ > 1) {
                    return false;
                }
            }
        }

        return true;
    }
}