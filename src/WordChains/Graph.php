<?php
namespace WordChains;

class Graph
{
    private $v;
    private $e;
    private $adj;

    /**
     * Initializes an empty graph with v vertices and 0 edges.
     *
     * @param $v number of vertices
     */
    public function __construct($v)
    {
        $this->v = $v;
        $this->e = 0;
        $this->adj = array();
    }

    /**
     * Returns the number of vertices in this graph
     *
     * @return int
     */
    public function getV()
    {
        return $this->v;
    }

    /**
     * Returns the number of edges in this graph
     *
     * @return int
     */
    public function getE()
    {
        return $this->e;
    }

    /**
     * Returns the vertices adjacent to vertex v
     *
     * @param $v
     * @return mixed
     * @throws \Exception
     */
    public function getAdj($v)
    {
        $this->validateVertex($v);

        return $this->adj[$v];
    }

    /**
     * Adds the undirected edge v-w to this graph.
     *
     * @param $v one vertex in the edge
     * @param $w the other vertex in the edge
     * @throws \Exception
     */
    public function addEdge($v, $w)
    {
        $this->validateVertex($v);
        $this->validateVertex($w);
        $this->e++;
        $this->adj[$v][] = $w;
        $this->adj[$w][] = $v;
    }

    /**
     * Validate vertex unless 0 <= v < V
     *
     * @param $v
     * @throws \Exception
     */
    private function validateVertex($v)
    {
        if ($v < 0 || $v >= $this->v) {
            throw new \Exception("vertex " + $v + " is not between 0 and " + ($this->v - 1));
        }
    }
}